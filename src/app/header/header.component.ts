import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLoggedIn = false;
  constructor(private auth: AuthServiceService, private router:Router) { 
    this.isLoggedIn = auth.loggedIn;
  }

  ngOnInit() {
  }

  logout(){
    this.auth.logout();
    console.log("Logging Out");
    this.router.navigate(['/'])
  }

}
