export  interface User {
    username: String;
    email: String;
    firstname: String;
    lastname: String;
    middlename: String;
    password: String;

}