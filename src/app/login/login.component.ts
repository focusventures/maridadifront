import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from './../services/auth-service.service';
import { Observable } from 'rxjs';
import { map, first } from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  LoginForm: FormGroup;
  error = '';
  constructor(private formbuilder: FormBuilder, private authUser:AuthServiceService, private router:Router) { 
    this.CreateForm();
    console.log("Login Component");
  }

  ngOnInit() {
  }

  CreateForm(){
    this.LoginForm = this.formbuilder.group({
      username: ['', Validators.required],
      // email: ['', Validators.required],
      password: ['', Validators.required],
     
    })
  }

public submit() {
  let userinfo = {
    'username': this.LoginForm.controls['username'].value,
    'password': this.LoginForm.controls['password'].value,
  }
  this.authUser.login(userinfo).subscribe(result => {
      console.log(result);
    
      this.router.navigate(['/'])
    },
  
    err => this.error = "Could Not authenticate"
  )
}

}
