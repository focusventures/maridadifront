import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ListViewComponent } from './home/list-view/list-view.component';
import { DetailViewComponent } from './home/detail-view/detail-view.component';
import  { EditViewComponent } from './home/edit-view/edit-view.component';
import { CheckoutComponent } from './home/checkout/checkout.component';
import { LogoutComponent } from './logout/logout.component';
import { NewProductComponent } from './new-product/new-product.component';
import { AuthGuard } from '../app/auth.guard';

const routes: Routes = [
    
  {
    path: '',  
    component: HomeComponent,
    children: [
      {
        path: '',  
        component: ListViewComponent,
      },
        
  {
    path: 'list',  
    component: ListViewComponent,
  },
    
  {
    path: 'detail/:id',
    canActivate: [AuthGuard],  
    component: DetailViewComponent,
  },
  
    
  {
    path: 'update/:id',
    canActivate: [AuthGuard],  
    component: EditViewComponent,
  },
    
  {
    path: 'checkout',  
    canActivate: [AuthGuard],  
    component: CheckoutComponent,
  },
  {
    path: 'newproduct',
    canActivate: [AuthGuard],    
    component: NewProductComponent,
  }
    ]

    
  },
 

  
  {
    path: 'login',  
    component: LoginComponent,
  }
  ,
 

  
  {
    path: 'logout',  
    component: LogoutComponent,
  }
  ,
    
  {
    path: 'register',  
    component: RegisterComponent,
  },
    
  {
    path: 'profile',  
    canActivate: [AuthGuard],  
    component: ProfileComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {



 }
