import { Component, OnInit } from '@angular/core';
import { ProductsServiceService } from '../../services/products-service.service';
import { Router,  ActivatedRoute, ParamMap  } from '@angular/router';
import { FormGroup, FormBuilder, Validators  } from '@angular/forms';


import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-edit-view',
  templateUrl: './edit-view.component.html',
  styleUrls: ['./edit-view.component.scss']
})
export class EditViewComponent implements OnInit {

  product = {id: '', name:'', description:"", price:"", image1:"", image2:"", image3:""};
  id = "";
  ProductForm: FormGroup;
  constructor(private productsApi:ProductsServiceService, private router:Router,   private route: ActivatedRoute, private formbuilder: FormBuilder,) {
    
    this.route.paramMap.subscribe(params => {
      this.getProduct(params.get("id"));
      this.CreateForm();
      
    })
   }

  ngOnInit() {
  }

  getProduct(id){
    this.productsApi.getAProduct(id).subscribe(data => {
      console.log(data);
      this.product = data;
    }, err => {
      console.log(err);
    })
  }


  
  CreateForm(){
    this.ProductForm = this.formbuilder.group({
      name: [this.product.name, Validators.required],
      price: [this.product.price, Validators.required],
      description: [this.product.description, Validators.required],
      image1: [this.product.image1, Validators.required],
      image2: [this.product.image2, Validators.required],
      image3: [this.product.image3, Validators.required],
    })
  }

  updateProduct(){
    let product = {
      'name': this.ProductForm.controls["name"].value,
      'price':this.ProductForm.controls["price"].value,
      'description': this.ProductForm.controls["description"].value,
      'image1': this.ProductForm.controls["image1"].value,
      'image2':this.ProductForm.controls["image2"].value,
      'image3':this.ProductForm.controls["image3"].value
    }
    this.productsApi.updateProduct(this.product.id, product).subscribe(data => {
      console.log(data);
      this.router.navigateByUrl('/')
    }, err => {
      console.log(err);
    })

  }


}
