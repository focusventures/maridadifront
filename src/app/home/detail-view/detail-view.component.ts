import { Component, OnInit } from '@angular/core';
import { ProductsServiceService } from '../../services/products-service.service';
import { Router,  ActivatedRoute, ParamMap  } from '@angular/router';

import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class DetailViewComponent implements OnInit {

  product = {id: '', name:'', description:"", price:"", image1:"", image2:"", image3:""};
  id = "";
  constructor(private productsApi:ProductsServiceService, private router:Router,   private route: ActivatedRoute) {
    
    this.route.paramMap.subscribe(params => {
      this.getProduct(params.get("id"))
    })

    
   }

  ngOnInit() {
    // this.product = this.route.paramMap.pipe(
    //   switchMap((params: ParamMap) =>
    //     this.productsApi.getAProduct(params.get('id')))
    // );
  }

  getProduct(id){
    this.productsApi.getAProduct(id).subscribe(data => {
      console.log(data);
      this.product = data;
    }, err => {
      console.log(err);
    })
  }

}
