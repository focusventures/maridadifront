import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListViewComponent } from './list-view/list-view.component';
import { DetailViewComponent } from './detail-view/detail-view.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { JumbotronComponent  } from './jumbotron/jumbotron.component';
import { EditViewComponent } from './edit-view/edit-view.component';
import { OwlModule } from 'ngx-owl-carousel';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    
    ListViewComponent,
    DetailViewComponent,
    CheckoutComponent,
    JumbotronComponent,
    EditViewComponent
   

   
  ],
  imports: [
    CommonModule,
    OwlModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule
  ]
})
export class HomeModule { }
