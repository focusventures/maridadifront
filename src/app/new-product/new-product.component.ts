import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { ProductsServiceService } from '../services/products-service.service';

import { Router } from '@angular/router';


@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {

  ProductForm: FormGroup;
  constructor(private formbuilder: FormBuilder, private productsApi: ProductsServiceService, private router:Router) { 

    this.CreateForm();
  }

  ngOnInit() {
  }

  CreateForm(){
    this.ProductForm = this.formbuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      description: ['', Validators.required],
      image1: ['', Validators.required],
      image2: ['', Validators.required],
      image3: ['', Validators.required],
    })
  }

  addProduct(){
    let product = {
      'name': this.ProductForm.controls["name"].value,
      'price':this.ProductForm.controls["price"].value,
      'description': this.ProductForm.controls["description"].value,
      'image1': this.ProductForm.controls["image1"].value,
      'image2':this.ProductForm.controls["image2"].value,
      'image3':this.ProductForm.controls["image3"].value
    }
    this.productsApi.addProduct(product).subscribe(data => {
      console.log(data);
      this.router.navigateByUrl('/')
    }, err => {
      console.log(err);
    })

  }

}
