import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { UserServiceService } from '../services/user-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {


  RegistrationForm: FormGroup;


  constructor(private formbuilder: FormBuilder, private userApi: UserServiceService, private router:Router) { 


    this.CreateForm();
  }

  ngOnInit() {
  }

  CreateForm(){
    this.RegistrationForm = this.formbuilder.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      firstname: ['', Validators.required],
      middlename: ['', Validators.required],
      lastname: ['', Validators.required],
      password: ['', Validators.required],
      confirm_password: ['', Validators.required]
    })
  }

  register(){
    let user = {
      'username': this.RegistrationForm.controls["username"].value,
      'email':this.RegistrationForm.controls["email"].value,
      'firstname': this.RegistrationForm.controls["firstname"].value,
      'lastname': this.RegistrationForm.controls["lastname"].value,
      'middlename':this.RegistrationForm.controls["middlename"].value,
      'password':this.RegistrationForm.controls["password"].value
    }
    this.userApi.registerUser(user).subscribe(data => {
      console.log(data["token"]);
      localStorage.setItem('token', data["token"]);
      this.router.navigateByUrl('/')


    }, err => {
      console.log(err);
    })

  }

}
