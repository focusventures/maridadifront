import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class SimpleInterceptor implements HttpInterceptor {

  constructor(private injector: Injector) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {    
            const JWT = `Bearer ${localStorage.getItem('token')}`;
            req = req.clone({
              setHeaders: {
                Authorization: JWT,
              },
            });
          
          return next.handle(req);
          }
}
