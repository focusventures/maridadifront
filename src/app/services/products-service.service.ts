import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsServiceService {

  constructor(private http: HttpClient) { }

  getProducts(): Observable<any>{
    return this.http.get(environment.express_root_uri + 'products');

  }

  addProduct(product):Observable<any>{
    return this.http.post(environment.express_root_uri + 'products/add', product, {});

  }

  
  updateProduct(id, product):Observable<any>{
    return this.http.post(environment.express_root_uri + 'products/update/' + id, product, {});

  }

  getAProduct(id):Observable<any>{
    console.log("Called Here");
    return this.http.get(environment.express_root_uri + 'products/edit/' +  id);

  }

  delete(product){
    return this.http.get(environment.express_root_uri + 'products/delete/' + product._id)
  }
}
