import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor( private http: HttpClient) { }


  loginUser(info): Observable<any>{
    return this.http.post(environment.express_root_uri + 'login',info, {});

  }

  public get loggedIn():boolean {
    return (localStorage.getItem('token') !== null);
  }

  login(userinfo): Observable<boolean>{
   
    return this.http.post(environment.express_root_uri + 'auth/', userinfo).pipe(
      map(result => {
        console.log(result["token"]);
        localStorage.setItem('token', result["token"]);
        return true;
      })
    )

  }

  logout(){
    localStorage.removeItem('token');
  }
}
