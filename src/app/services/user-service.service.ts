import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { User } from '../Models/User';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) 
  { }

  registerUser(user): Observable<any>{
    return this.http.post(environment.express_root_uri + 'users/add', user, {});

  }
}
