import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';

import { RegisterComponent } from './register/register.component';


import { HomeModule } from './home/home.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';


import { AuthServiceService } from './services/auth-service.service';
import { ProductsServiceService } from './services/products-service.service';
import { UserServiceService } from './services/user-service.service';


import { OwlModule } from 'ngx-owl-carousel';
import { NewProductComponent } from './new-product/new-product.component';

import {  JwtModule } from '@auth0/angular-jwt';
import { SimpleInterceptor } from './interceptors/interceptors';
import { AuthGuard } from '../app/auth.guard';
import { LogoutComponent } from './logout/logout.component';

export function tokenGetter(){
  return localStorage.getItem('token');
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    HomeComponent,
    RegisterComponent,
    HeaderComponent,
    FooterComponent,
    NewProductComponent,
    LogoutComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    OwlModule,
    JwtModule.forRoot({
      config: {
        tokenGetter:tokenGetter,
        whitelistedDomains: [
          'localhost:4000'
        ],
        blacklistedRoutes: [
          'localhost:4000/auth'
        ],

      }
    }),
    HomeModule
  ],
  providers: [UserServiceService,
    ProductsServiceService,
    AuthServiceService, 
    AuthGuard,
    {provide:HTTP_INTERCEPTORS,
      useClass:SimpleInterceptor,
        multi:true
    }, ],
  bootstrap: [AppComponent]
})
export class AppModule { }
